#!/bin/sh
# update "firefox versions"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh
. $(dirname $0)/functions/functions-section-releases.sh

# releases to verify
distributions_firefox="stable oldstable"

# server
server="http://mozilla.debian.net"

# types
types="aurora beta esr release"

# local function
# get alternate package file
get_alt_package_file(){
  curl -o ${file_local} --silent ${file_remote_alt}
  timestamp=$(local_file_get_last_modified ${file_local})
  $compress ${file_local}
  local_file_set_last_modified ${timestamp} ${file_local_compressed}
}

# get package file
get_package_file(){
  curl -o ${file_local_compressed_alt} --silent ${file_remote}
  timestamp=$(local_file_get_last_modified ${file_local_compressed_alt})
  gzip --force --decompress ${file_local_compressed_alt}
  $compress ${file_local}
  local_file_set_last_modified ${timestamp} ${file_local_compressed}
}

for distribution in $distributions_firefox; do
  release=$(get_release_codename ${distribution})

  for type in ${types}; do
    file_remote="${server}/dists/${release}-backports/firefox-${type}/binary-${arch}/Packages.gz"
    file_remote_alt="${server}/dists/${release}-backports/firefox-${type}/binary-${arch}/Packages"
    file_local="${path_downloads}/firefox-${type}-${distribution}-${arch}"
    file_local_compressed="${file_local}.xz"
    file_local_compressed_alt="${file_local}.gz"

    if [ -f ${file_local_compressed} ]; then
      # file is already downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        # use .gz file
        if [ "$(remote_file_get_last_modified ${file_remote})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          unlink  ${file_local_compressed}
          get_package_file
        fi
        elif curl --output /dev/null --silent --head --fail ${file_remote_alt}; then
        # use regular file
        if [ "$(remote_file_get_last_modified ${file_remote_alt})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          unlink  ${file_local_compressed}
          get_alt_package_file
        fi
      fi
    elif [ ! -f ${file_local_compressed} ]; then
      # file is not downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        get_package_file
      elif curl --output /dev/null --silent --head --fail ${file_remote_alt}; then
        get_alt_package_file
      fi
    fi
  done
done

store_current_timestamp "data-source-firefox-packages-update"
