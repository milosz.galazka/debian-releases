#!/bin/sh
# HTML functions

# generate html page header
html_page_header() {
  title=$1
cat << EOF
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8"/>
    <title>${title}</title>
  </head>
  <body>
  <header>
    <div class="container">
      <div>Debian releases</div>
      <nav class="navigation">
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/releases/">Releases</a></li>
          <li><a href="/versions/">Versions</a></li>
          <li><a href="/updates/">Updates</a></li>
          <li><a href="/newly-added/">New packages</a></li>
          <li><a href="/rss/">RSS</a></li>
        </ul>
      </nav><!-- End navigation -->
    </div><!-- End container -->
  </header><!-- End header -->
  <div class="sections">
    <div class="container">
      <div class="row">
        <div class="main-content">
EOF
}

# generate section header
html_section_header() {
  title=$1
cat << EOF
<article class="post page">
  <header class="entry-header post-head">
    <h2 class="entry-title">${title}</h2>
  </header>
  <div class="post-wrap">
    <div class="post-inner">
      <div class="entry-content">
EOF
}

# generate section footer
html_section_footer() {
cat << EOF
      <br/><hr/><br/>
      </div>
    </div><!-- End post-inner -->
  </div><!-- End post-wrap -->
</article>        
EOF
}

# generate html page footer
html_page_footer() {
cat << EOF
        </div><!-- End main-content -->
      </div><!-- End row -->
    </div><!-- End container -->
  </div><!-- End sections -->
  </body>
</html>
EOF
}
