#!/bin/sh

. ${path_functions}/functions-database.sh
. ${path_functions}/functions-changelogs.sh
. ${path_functions}/functions-html-page.sh
. ${path_functions}/functions-section-information.sh
. ${path_functions}/functions-section-newly-added.sh
. ${path_functions}/functions-section-package-versions.sh
. ${path_functions}/functions-section-proposed-updates.sh
. ${path_functions}/functions-section-releases.sh
. ${path_functions}/functions-section-security-updates.sh
. ${path_functions}/functions-section-updates.sh
. ${path_functions}/functions-section-rss.sh
. ${path_functions}/functions-section-firefox.sh

if [ -f "${path_main}/overrides/functions-html-page.sh" ]; then
  . ${path_main}/overrides/functions-html-page.sh
fi
