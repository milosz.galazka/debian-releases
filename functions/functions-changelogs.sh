#!/bin/sh
# functions used to extract data

# get last changelog entry for package
# use full changelog
# this function is currently obsolete 
get_last_changelog_for_package_and_version(){
  package=$1
  version=$2
  distribution=$3

  pretty_version=$(echo ${version} | awk -F\: '{print $NF}')

  package_file_local="${path_downloads}/packages-${distribution}-${component}-${arch}"
  package_file_local_compressed="${package_file_local}.xz"

  basefile=$(extract_field_for_package ${package} "Filename" ${package_file_local_compressed})
  directory=$(dirname ${basefile} | sed 's/pool\///')

  alt_package=$(extract_field_for_package ${package} "Source" ${package_file_local_compressed} | awk '{print $1}')

  if [ -n "${alt_package}" ]; then
    real_package=${alt_package}
  else
    real_package=${package}
  fi

  file_local="${path_changelogs}/${directory}/${real_package}_${pretty_version}_changelog"
  file_local_compressed="${file_local}.xz"

  if [ -f "${file_local_compressed}" ]; then
    xzcat ${file_local_compressed} | sed -n "/${real_package} (${version})/,/^${real_package}/ {/^${real_package}/!p}" | sed 's/<.*>//'
  fi
}

# get last changelog entry for package
# use already parsed file as this function is w/o sed part
get_changelog_entry_for_package_and_version(){
  package=$1
  version=$2

  package_file_local="${path_downloads}/packages-${distribution}-${component}-${arch}"
  package_file_local_compressed="${package_file_local}.xz"
  security_file_local="${path_downloads}/security-updates-${distribution}-${component}-${arch}"
  security_file_local_compressed="${security_file_local}.xz"


  # check "Source" package for specific version
  alt_package=$(extract_field_for_package_and_version ${package} ${version} "Source" ${package_file_local_compressed} | awk '{print $1}')
  if [ -n "${alt_package}" ]; then
    alt_version=$(extract_field_for_package_and_version ${package} ${version} "Source" ${package_file_local_compressed} | sed  -n '/.*(.*)/ {s/.* (\(.*\))/\1/p}')
    if [ -n "${alt_version}" ]; then
      real_version="${alt_version}"
    else
      real_version="${version}"
    fi
    real_package="${alt_package}"
  else
    # check security updates for specific "Source" package
    alt_package=$(extract_field_for_package_and_version ${package} ${version} "Source" ${security_file_local_compressed} | awk '{print $1}')
    if [ -n "${alt_package}" ]; then
      alt_version=$(extract_field_for_package_and_version ${package} ${version} "Source" ${security_file_local_compressed} | sed  -n '/.*(.*)/ {s/.* (\(.*\))/\1/p}')
      if [ -n "${alt_version}" ]; then
        real_version="${alt_version}"
      else
        real_version="${version}"
      fi
      real_package="${alt_package}"
    else
      # there is no specific "Source" package
      real_package="${package}"
      real_version="${version}"
    fi
  fi


  basefile=$(extract_field_for_package_and_version ${package} ${version} "Filename" ${package_file_local_compressed})
  if [ -z "${basefile}" ]; then
    basefile=$(extract_field_for_package_and_version ${package} ${version} "Filename" ${security_file_local_compressed})
    directory=$(dirname ${basefile} | sed 's/pool\/updates\///')
  else
    directory=$(dirname ${basefile} | sed 's/pool\///')
  fi

  pretty_version=$(echo ${real_version} | awk -F\: '{print $NF}')

  file_local="${path_changelogs}/${directory}/${real_package}_${pretty_version}_changelog"
  file_local_compressed="${file_local}.xz"

  if [ -f "${file_local_compressed}" ]; then
    xzcat ${file_local_compressed} | sed 's/<.*>//'
  fi 
}
