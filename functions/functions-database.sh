#!/bin/sh
# functions used to extract data

# get package names
local_file_get_packages(){
  local_file=$1
  ${decompress_to_stdout} ${local_file} | awk -v FS=": " '/^Package/  {print $2}'
}

# get cached field for defined package
get_cached_field_for_package(){
  package=$1
  field=$2
  local_file=$3

  case "$field" in
    "package")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $1}' ;;
    "component")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $2}' ;;
    "version")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $3}' ;;
    "file")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $4}' ;;
    "description")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $5}' ;;
    "updated")
      ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $6}' ;;
    "updated_pretty")
      TZ=UTC LC_ALL=C date -d "@$(${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $6}')" ;;
  esac
}

# get cached field for defined package using uncompressed file
get_uncompressed_cached_field_for_package(){
  package=$1
  field=$2
  local_file=$3

  case "$field" in
    "package")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $1}' ${local_file} ;;
    "component")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $2}' ${local_file} ;;
    "version")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $3}' ${local_file} ;;
    "file")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $4}' ${local_file} ;;
    "description")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $5}' ${local_file} ;;
    "updated")
      awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $6}' ${local_file} ;;
    "updated_pretty")
      TZ=UTC LC_ALL=C date -d "@$(awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $6}' ${local_file})" ;;
  esac
}


# extract field for defined package
extract_field_for_package(){
  package=$1
  field=$2
  local_file=$3

  ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v ORS="\n" -v FS="\n" -v RS="" 'split($1,var,": ")  var[1] ~ /Package/ && var[2] == appname  { for (i=1;i<=NF;i++) {last=split($i, subfield, ": ");name = subfield[1];value = subfield[last];record[name] = value;} print record["'${field}'"]}' | sort -h -r | head -1
}

# extract field for defined package and its version
extract_field_for_package_and_version(){
  package=$1
  version=$2
  field=$3
  local_file=$4

  ${decompress_to_stdout} ${local_file} | awk -v appname="${package}" -v ORS="\n" -v FS="\n" -v RS="" 'split($1,var,": ") var[1] ~ /Package/ && var[2] == appname {for (i=1;i<=NF;i++) {last=split($i, subfield, ": "); name = subfield[1]; value = subfield[last]; record[name] = value;} if (record["Version"] == "'${version}'") print record["'${field}'"]}' 
}
