#!/bin/sh
# firefox section

# releases to verify
distributions_firefox="stable oldstable"

# types
types="aurora beta esr release"

firefox_describe(){
  type=$1
  case ${type} in
    "esr") echo "Extended Support Release" ;;
    "beta") echo "Beta" ;;
    "aurora") echo "Aurora" ;;
    "release") echo "Release" ;;
  esac
}
generate_section_firefox() {
  echo "<div id=\"versions-firefox\"></div>"
  html_section_header "Firefox versions"
  for distribution in $distributions_firefox; do
    release=$(get_release_codename ${distribution} "pretty")
    echo "<p><strong>Debian $release ($distribution)</strong></p>"
    for type in ${types}; do
      if [ "${type}" = "esr" ]; then
        package="firefox-esr"
      else
        package="firefox"
      fi
      file_local_compressed="${path_downloads}/firefox-${type}-${distribution}-${arch}.xz"
      if [ -f "${file_local_compressed}" ]; then
        version=$(extract_field_for_package ${package} "Version" ${file_local_compressed})
# Extended Support Release  
        echo "<p>Mozilla Firefox <code>$(firefox_describe ${type})</code> version is <code>${version}</code></p>"
      fi
    done
  done
  html_section_footer
}
