#!/bin/sh
# newly added section

# get defined number of records
get_new_packages_top(){
  distribution=$1
  top_records=$2
  file_local_compressed="${path_cache}/newly-added-${distribution}.xz"

  ${decompress_to_stdout} ${file_local_compressed} | head -${top_records} | awk  -v FS="|" -v RS="\n" '{print $1}'
}

# parse cache file to get desired field
get_new_package_field_for(){
  distribution=$1
  package=$2
  field=$3
  file_local_compressed="${path_cache}/newly-added-${distribution}.xz"

  case "$field" in
    "package")
      ${decompress_to_stdout} ${file_local_compressed} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $1}' ;;
    "url")
      ${decompress_to_stdout} ${file_local_compressed} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $2}' ;;
    "description")
      ${decompress_to_stdout} ${file_local_compressed} | awk -v appname="${package}" -v FS="|" -v RS="\n" '$1 == appname {print $3}' ;;
  esac
}

# generate table of contents
generate_section_new_toc() {
  html_section_header "Table of contents"
  echo "<p><strong>Recently added packages</strong></p>"
  for distribution in $distributions_new; do
    echo "<p><a href=\"#new-${distribution}\">Packages added to Debian $(get_release_codename "$distribution" "pretty") ($distribution) during last 7 days</a></p>"
  done
  html_section_footer
}

# generate list of new packages
generate_section_new_packages() {
  for distribution in $distributions_new; do
    new_packages=$(get_new_packages_top "$distribution" 100)

    echo "<div id=\"new-${distribution}\"></div>"
    html_section_header "Packages added to Debian $(get_release_codename "$distribution" "pretty") ($distribution) during the last 7 days"
    for package in $new_packages; do
        description=$(get_new_package_field_for "$distribution" "$package" "description")
        link=$(get_new_package_field_for "$distribution" "$package" "url")
        echo "<p><a rel=\"external nollow\" target=\"_blank\" href=\"${link}\">Inspect</a> package <code data-toggle=\"tooltip\" title=\"${description}\">${package}</code></p>"
    done
    html_section_footer
done
}
