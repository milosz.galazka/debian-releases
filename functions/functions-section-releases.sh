#!/bin/sh
# releases section

# parse release file to get codename
get_release_codename() {
  distribution=$1
  option=$2
  file_local_compressed="${path_downloads}/release-${distribution}.xz"

  if [ -n "${distribution}" -a -n "${option}" -a "${option}" = "pretty" ]; then
    ${decompress_to_stdout} ${file_local_compressed} | awk -v FS=": " '/Codename/ {print toupper(substr($2,1,1)) substr($2,2);}' 
  elif [ -n "${distribution}" ]; then
    ${decompress_to_stdout} ${file_local_compressed} |awk -v FS=": " '/Codename/ {print $2}' 
  fi
}

# parse release file to get version
get_release_version() {
  distribution=$1
  file_local_compressed="${path_downloads}/release-${distribution}.xz"

  ${decompress_to_stdout} ${file_local_compressed} | awk -v FS=": " '/Version/ {print $2}'
}

# parse release file to get date
get_release_date() {
  distribution=$1
  file_local_compressed="${path_downloads}/release-${distribution}.xz"

  ${decompress_to_stdout} ${file_local_compressed} | awk -v FS=": " '/Date/ {print $2}' | sed "s/.*, \(.*\) .* UTC/\1/"
}

# generate brief section (w/o unstable, oldoldstable only if available)
generate_section_brief_releases(){
  html_section_header "Debian releases"
  for distribution in $distributions_all; do
    release=$(get_release_codename ${distribution})
    version=$(get_release_version ${distribution})
    date=$(get_release_date ${distribution})
    case "${distribution}" in
      "testing")
        echo "<p>The <code>${distribution}</code> suite is the next generation <code>${release}</code> release</p>" ;;
      "stable")
        echo "<p>The <code>${distribution}</code> suite is the current stable <code>${release}</code> release, recent version <code>${version}</code> was published on <code>${date}</code></p>" ;;
      "oldstable")
        echo "<p>The <code>${distribution}</code> suite is the previous stable <code>${release}</code> release, latest version <code>${version}</code> was published on <code>${date}</code></p>" ;;
      "oldoldstable")
        echo "<p>The <code>${distribution}</code> suite is the previous release before last, <code>${release}</code> release, latest version <code>${version}</code> was published on <code>${date}</code></p>" ;;
    esac
  done
  html_section_footer
}

# generate section
generate_section_releases() {
  html_section_header "Debian releases"
  for distribution in $distributions_all; do
    release=$(get_release_codename ${distribution})
    version=$(get_release_version ${distribution})
    date=$(get_release_date ${distribution})
    case "$distribution" in
      "testing")
        echo "<p>The <code>${distribution}</code> suite is the next generation <code>${release}</code> release</p>" ;;
      "unstable")
        echo "<p>The <code>${distribution}</code> suite is the unstable development <code>${release}</code> release</p>";;
      "stable")
        echo "<p>The <code>${distribution}</code> suite is the current stable <code>${release}</code> release, recent version <code>${version}</code> was published on <code>${date}</code></p>" ;;
      "oldstable")
        echo "<p>The <code>${distribution}</code> suite is the previous stable <code>${release}</code> release, latest version <code>${version}</code> was published on <code>${date}</code></p>" ;;
      "oldoldstable")
        echo "<p>The <code>${distribution}</code> suite is the previous release before last, <code>${release}</code> release, latest version <code>${version}</code> was published on <code>${date}</code></p>" ;;
    esac
  done
  if [ "${distribution_oldoldstable_is_removed}" -eq 1 ]; then
    echo "<p>The <code>oldoldstable</code> suite was not found on mirror server, it was moved to <a rel="external" target="_blank" href="http://archive.debian.org/">Debian Archive</a></p>"
  fi
  html_section_footer
}
