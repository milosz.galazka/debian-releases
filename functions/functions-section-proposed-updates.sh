#!/bin/sh
# proposed updates section

# get defined number of records
get_proposed_updates_top(){
  distribution=$1
  top_records=$2
  file_local_compressed="${path_cache}/proposed-updates-${distribution}-${component}-${arch}.xz"

  ${decompress_to_stdout} ${file_local_compressed} | sort -t "|" -k 6 -n -r | head -${top_records}| awk  -v FS="|" -v RS="\n" '{print $1}'
}

# generate list of proposed updates
generate_section_proposed_updates() {
for distribution in ${distributions_updates}; do

    echo "<div id=\"proposed-update-${distribution}\"></div>"
    html_section_header "Debian $(get_release_codename ${distribution} "pretty") (${distribution}) proposed update channel"

    for component in $components; do
      updated_packages=$(get_proposed_updates_top ${distribution} 100)
      file_local_compressed="${path_cache}/proposed-updates-${distribution}-${component}-${arch}.xz"

      echo "<p class=\"button component\"><strong>${component}</strong> component</p>"
      if [ -n "${updated_packages}" ]; then
        for package in $updated_packages; do
          component=$(get_cached_field_for_package "${package}" "component" ${file_local_compressed})
          version=$(get_cached_field_for_package "${package}" "version" ${file_local_compressed})
          description=$(get_cached_field_for_package "${package}" "description" ${file_local_compressed})
          date=$(get_cached_field_for_package "${package}" "updated_pretty" ${file_local_compressed})
          echo "<p>Package <code data-toggle=\"tooltip\" title=\"${description}\">${package}</code> was proposed on <code>${date}</code> to version <code>${version}</code></p>"
        done
      else
        echo "<p>none</p>"
      fi
    done
    html_section_footer
done
}

