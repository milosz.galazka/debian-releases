#!/bin/sh
# package versions section

# parse downloaded file to get defined field
get_software_version_field_for() {
  distribution=$1
  package=$2
  field=$3
  file_local_compressed="${path_downloads}/packages-${distribution}-${component}-${arch}.xz"

  case "${field}" in
    "description")
      echo $(extract_field_for_package ${package} "Description" ${file_local_compressed}) ;;
    "homepage")
      case "${package}" in
        "linux-image-amd64") 
          echo "https://www.kernel.org"
          ;;
        "bind9")
          echo "http://www.bind9.net"      
          ;;
        "postgresql")
          echo "http://www.postgresql.org"
          ;;
        "xserver-xorg")
          echo "http://www.x.org"
          ;;
        "gcc")
          echo "https://gcc.gnu.org"
          ;;
        *) 
          echo $(extract_field_for_package ${package} "Homepage" ${file_local_compressed})
      esac
      ;;
    "version")
      if [ "${distribution}" != "testing" -a "${distribution}" != "unstable" ]; then
        version=""
        for component in ${components}; do
          file_local_security_compressed="${path_cache}/security-updates-${distribution}-${component}-${arch}.xz"
          local_version=$(get_cached_field_for_package ${package} "version" ${file_local_security_compressed})
          if [ -n "${local_version}" ]; then
            version=${local_version}
          fi
        done
        if [ -z "${version}" ]; then
          version=$(extract_field_for_package ${package} "Version" ${file_local_compressed})
        else
          # security update is not always the most recent version
          version_secondary=$(extract_field_for_package ${package} "Version" ${file_local_compressed})
          version=$(echo "${version}\n${version_secondary}" | sort -hr | head -1)
        fi
        echo ${version}
      else
        echo $(extract_field_for_package ${package} "Version" ${file_local_compressed})
      fi
      ;;
  esac
}

# set css class according to distribution
set_package_version_color(){
  distribution=$1
  case "$distribution" in
    "testing")
      echo "warning";;
    "unstable")
      echo "danger";;
    "stable")
      echo "success";;
    "oldstable")
      echo "success";;
    "oldoldstable")
      echo "info";;
  esac
}

# generate package descriptions
generate_section_package_versions_descriptions() {
  echo "<div id=\"versions-desc\"></div>"
  html_section_header "Monitored packages"

  # use main component
  component="main"

  package_number=1;
  for package in ${packages_to_check}; do
    distribution="stable"
    file_local="${path_downloads}/packages-${distribution}-${component}-${arch}"
    file_local_compressed="${file_local}.xz"

    description=$(get_software_version_field_for ${distribution} ${package} "description") 
    homepage=$(get_software_version_field_for ${distribution} ${package} "homepage")

    echo "<div id=\"${package}\">"
    echo -n "<p><sup>[${package_number}]</sup> <code>${package}</code>"

    alt_package=$(extract_field_for_package ${package} "Source" ${file_local_compressed} | awk '{print $1}')

    if [ -n "${alt_package}" ]; then
      real_package="${alt_package}"
    else
      real_package="${package}"
    fi

    echo -n "<span class=\"post-more\"><a data-toggle="tooltip" title=\"${real_package} homepage is ${homepage}\" rel=\"external nofollow\" target=\"_blank\" href=\"${homepage}\"><i class=\"fa fa-home\"></i></a> <a data-toggle="tooltip" title=\"This service lets you follow current ${real_package} bug reports\" rel=\"external nofollow\" target=\"_blank\" href=\"https://bugs.debian.org/${real_package}\"><i class=\"fa fa-bug\"></i></a> <a data-toggle="tooltip" title=\"This service lets you follow the evolution of ${real_package} Debian package\"  rel=\"external nofollow\" target=\"_blank\" href=\"https://tracker.debian.org/pkg/${real_package}\"><i class=\"fa fa-heartbeat\"></i></a> <a data-toggle="tooltip" title=\"This service lets you follow current ${real_package} security issues\"  rel=\"external nofollow\" target=\"_blank\" href=\"https://security-tracker.debian.org/tracker/source-package/${real_package}\"><i class=\"fa fa-ambulance\"></i></a></span></p>"
    echo -n "<p>${description}</p>"

    # get changelog entry
    version=$(get_software_version_field_for ${distribution} ${package} "version")
    changelog_entry=$(get_changelog_entry_for_package_and_version ${package} ${version})
    if [ -n "${changelog_entry}" ]; then
      echo "<pre>${changelog_entry}</pre>"
    else
      echo "<pre>changelog not available</pre>"
    fi

    echo "</div>"
    package_number=$(expr ${package_number} \+ 1)
  done

  html_section_footer
}

# generate package version table
generate_section_package_versions_table(){
  # use main component
  component="main"

  echo "<div id=\"versions-table\"></div>"
  html_section_header "Package version comparison"

  # package versions tab;e
  echo -n "<div style=\"overflow:auto\"><table class=\"table table-condensed\">"
  # header
  echo -n "<thead><tr>"
  echo -n "<th>Package/Release</th>"
  for distribution in ${distributions_all}; do
    echo -n "<th>Debian $(get_release_codename ${distribution} "pretty") (${distribution})</th>"
  done
  echo "</tr></thead>"

  package_number=1;
  for package in ${packages_to_check}; do
    echo -n "<tr>"
    echo -n "<td><code>${package}</code> <sup><a href=\"#${package}\">[${package_number}]</a></sup></td>"

    for distribution in $distributions_all; do
      echo -n "<td class=\"$(set_package_version_color ${distribution})\">$(get_software_version_field_for ${distribution} ${package} "version")</td>"
    done
    echo "</tr>"

    package_number=$(expr ${package_number} \+ 1)
  done
  echo "</table></div>"
  echo "<p>Please read <a rel=\"external nofollow\" target=\"_blank\" href=\"https://wiki.debian.org/LTS/\">Debian Long Term Support</a> document for accurate information on long term support.</p>"
  if [ ${distribution_oldoldstable_is_removed} -eq 1 ]; then
    echo "<p>The <code>oldoldstable</code> suite was not found on mirror server, it was moved to <a rel="external" target="_blank" href="http://archive.debian.org/">Debian Archive</a></p>"
  fi
  html_section_footer
}


# table of contents
# generate table of contents
generate_section_package_versions_toc() {
  html_section_header "Table of contents"
  echo "<p><strong>Basic software</strong></p>"
  echo "<p><a href=\"#versions-table\">Package version comparison</a></p>"
  echo "<p><a href=\"#versions-desc\">Monitored packages</a></p>"

  echo "<p><strong>Additional software</strong></p>"
  echo "<p><a href=\"#versions-firefox\">Firefox versions</a></p>"
  html_section_footer
}

