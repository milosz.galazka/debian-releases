#!/bin/sh
# functions used during update process

# check if remote file exists
# 0 - true
# 1 - false
remote_file_exists() {
  remote_file=$1
  if curl --output /dev/null --silent --head --fail  ${remote_file}; then
    echo "0"
  else
    echo "1"
  fi
}

# get "Last-Modified" time for remote file
remote_file_get_last_modified(){
  remote_file=$1
  if [ "$(remote_file_exists ${remote_file})" -eq 0 ]; then
    date=$(curl --silent --head ${remote_file} | awk -v ORS="" -v FS=": " -v RS="\n" '/Last-Modified: / {print $2}');
    echo $(date -d "${date}" +"%s")
  else
    echo "0"
  fi
}

# get "Last-Modified" time for local file
local_file_get_last_modified(){
  local_file=$1
  if [ -f "${local_file}" ]; then 
    echo $(stat --format "%Y" ${local_file})
  else
    echo "0"
  fi
}

# set "Last-Modified" time for local file
local_file_set_last_modified(){
  timestamp=$1
  local_file=$2
  touch -t "$(date -d "@${timestamp}" +%Y%m%d%H%M.%S)" ${local_file}
}

# store current timestamp
store_current_timestamp(){
  timestamp_name=$1
  TZ=UTC LC_ALL=C date > ${path_timestamps}/${timestamp_name}
}
