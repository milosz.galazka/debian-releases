#!/bin/sh
# functions used to indicate when page and data source was updated 

# print current date
current_date(){
  TZ=UTC LC_ALL=C date
}

# generate section to print when page and data source was updated
generate_section_information() {
  date_file=$1
  html_section_header "Information"
  if [ -n "${date_file}" ]; then
    echo "<p>Data source was updated on <code>$(cat ${path_timestamps}/${date_file})</code></p>"
  fi
  echo "<p>This page was created on <code>$(current_date)</code></p>"
  html_section_footer
}  

