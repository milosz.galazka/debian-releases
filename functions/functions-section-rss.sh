#!/bin/sh
# releases section

# generate section
generate_section_rss() {
  html_section_header "RSS feeds"

  types="security-updates proposed-updates updates"
  for distribution in ${distributions_updates}; do
    echo "<p><strong>Debian $(get_release_codename "$distribution" "pretty") ($distribution)</strong></p>"  
    for type in ${types}; do
      for component in $components; do
        file_local_compressed="${path_cache}/${type}-${distribution}-${component}-${arch}.xz"
         case ${type} in
          "updates")
            echo "<p><a href="/rss/${distribution}-updates-${component}.rss">Release updates for ${component} component</a></p>"
            ;;
          "proposed-updates")
            echo "<p><a href="/rss/${distribution}-proposed-updates-${component}.rss">Proposed updates for ${component} component</a></p>"
            ;;
          "security-updates")
            echo "<p><a href="/rss/${distribution}-security-updates-${component}.rss">Security updates for ${component} component</a></p>"
            ;;
          esac
      done
    done
  done

  html_section_footer
}
