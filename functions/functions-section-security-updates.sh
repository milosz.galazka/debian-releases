#!/bin/sh
# security section

# get defined number of records
get_security_updates_top(){
  distribution=$1
  top_records=$2

  ${decompress_to_stdout} ${file_local_compressed} | sort -t "|" -k 6 -n -r | head -${top_records} | awk  -v FS="|" -v RS="\n" '{print $1}'
}

# generate brief list of security updates
generate_section_brief_security(){
  for distribution in $distributions_security; do
    html_section_header "Recent security updates for Debian $(get_release_codename "$distribution" "pretty") ($distribution)"
    for component in ${components}; do
      file_local_compressed="${path_cache}/security-updates-${distribution}-${component}-${arch}.xz" 
      updated_packages=$(get_security_updates_top "$distribution" 10)

      echo "<p class=\"button component\"><strong>${component}</strong> component</p>"
      if [ -n "${updated_packages}" ]; then
        for package in $updated_packages; do
          version=$(get_cached_field_for_package ${package} "version" ${file_local_compressed})
          date=$(get_cached_field_for_package ${package} "updated_pretty" ${file_local_compressed})
          echo "<p>Package <code>${package}</code> was updated on <code>${date}</code> to version <code>${version}</code></p>"

          # get changelog entry
          changelog_entry=$(get_changelog_entry_for_package_and_version ${package} ${version})
          if [ -n "${changelog_entry}" ]; then
            echo "<pre>${changelog_entry}</pre>"
          else
            echo "<pre>changelog not available</pre>"
          fi
        done
      else
        echo "<p>none</p>"
      fi
    done
    html_section_footer
done
}

# generate table of contents
generate_section_updates_toc() {
  html_section_header "Table of contents"
  echo "<p><strong>Security updates</strong></p>"
  for distribution in $distributions_security; do
    echo "<p><a href=\"#security-${distribution}\">Debian $(get_release_codename "$distribution" "pretty") ($distribution) security updates</a></p>"
  done
  echo "<p><strong>Release updates</strong></p>"
  for distribution in $distributions_updates; do
    echo "<p><a href=\"#update-${distribution}\">Debian $(get_release_codename "$distribution" "pretty") ($distribution) release updates</a></p>"
  done
  echo "<p><strong>Proposed updates</strong></p>"
  for distribution in $distributions_updates; do
    echo "<p><a href=\"#proposed-update-${distribution}\">Debian $(get_release_codename "$distribution" "pretty") ($distribution) updates</a></p>"
  done
  html_section_footer
}

# generate list of security updates
generate_section_security_updates(){
  for distribution in $distributions_security; do
    echo "<div id=\"security-${distribution}\"></div>"
    html_section_header "Debian $(get_release_codename "$distribution" "pretty") ($distribution) security updates"
    for component in ${components}; do
      file_local_compressed="${path_cache}/security-updates-${distribution}-${component}-${arch}.xz" 
      updated_packages=$(get_security_updates_top "$distribution" 100)

      echo "<p class=\"button component\"><strong>${component}</strong> component</p>"
      if [ -n "${updated_packages}" ]; then
        for package in $updated_packages; do
          component=$(get_cached_field_for_package ${package} "component" ${file_local_compressed})
          version=$(get_cached_field_for_package ${package} "version" ${file_local_compressed})
          description=$(get_cached_field_for_package ${package} "description" ${file_local_compressed})
          date=$(get_cached_field_for_package ${package} "updated_pretty" ${file_local_compressed})
          echo "<p>Package <code data-toggle=\"tooltip\" title=\"${description}\">${package}</code> was updated on <code>${date}</code> to version <code>${version}</code></p>"

          # get changelog entry
          changelog_entry=$(get_changelog_entry_for_package_and_version ${package} ${version})
          if [ -n "${changelog_entry}" ]; then
            echo "<pre>${changelog_entry}</pre>"
          else
            echo "<pre>changelog not available</pre>"
          fi 
        done
      else
        echo "<p>none</p>"
      fi
    done
    html_section_footer
done
}

