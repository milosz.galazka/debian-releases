#!/bin/sh
# update "proposed updates"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh
. $(dirname $0)/functions/functions-database.sh

# local function
# get alternate package file
get_alt_package_file(){
  curl -o ${file_local_compressed_alt} --silent ${file_remote_alt}
  timestamp=$(local_file_get_last_modified ${file_local_compressed_alt})
  gzip --force --decompress ${file_local_compressed_alt}
  if [ -f ${file_local_compressed} ]; then
    unlink  ${file_local_compressed}
  fi
  $compress ${file_local}
  local_file_set_last_modified ${timestamp} ${file_local_compressed}
}

for distribution in $distributions_proposed_updates; do
  for component in $components; do
    file_remote="${server}/debian/dists/${distribution}-proposed-updates/${component}/binary-${arch}/Packages.xz"
    file_remote_alt="${server}/debian/dists/${distribution}-proposed-updates/${component}/binary-${arch}/Packages.gz"
    file_local="${path_downloads}/proposed-updates-${distribution}-${component}-${arch}"
    file_local_compressed="${file_local}.xz"
    file_local_compressed_alt="${file_local}.gz"

    if [ -f ${file_local_compressed} ]; then
      # file is already downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        # use .xz file
        if [ "$(remote_file_get_last_modified ${file_remote})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          unlink  ${file_local_compressed}
          curl -o ${file_local_compressed} --silent ${file_remote}
        fi
      else
        # use .gz file
        if [ "$(remote_file_get_last_modified ${file_remote_alt})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          get_alt_package_file
        fi
      fi
    elif [ ! -f ${file_local_compressed} ]; then
      # file is not downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        curl -o ${file_local_compressed} --silent ${file_remote}
      else
        get_alt_package_file
      fi
    fi
  done
done

for distribution in $distributions_proposed_updates; do
 for component in $components; do
   file_local="${path_downloads}/proposed-updates-${distribution}-${component}-${arch}"
   file_local_compressed="${file_local}.xz"
   file_cache="${path_cache}/proposed-updates-${distribution}-${component}-${arch}"
   file_cache_compressed="${file_cache}.xz"

   packages=$(local_file_get_packages ${file_local_compressed})

   if [ -f "${file_cache_compressed}" ]; then
     ${decompress_to_file} ${file_cache_compressed}
   else
     touch ${file_cache}
   fi

   for package in $packages; do
     version=$(extract_field_for_package ${package} "Version" ${file_local_compressed})
     filename=$(extract_field_for_package ${package} "Filename" ${file_local_compressed})
     description=$(extract_field_for_package ${package} "Description" ${file_local_compressed} | head -1)
     filename_cached=$(get_uncompressed_cached_field_for_package ${package} "file" ${file_cache})

     if xzgrep --silent "^${package}|" ${file_cache}; then
       if [ "${filename}" != "${filename_cached}" ]; then
         date=$(remote_file_get_last_modified ${server}/debian/${filename})
         sed -i "/^${package}|/d" ${file_cache}
         echo "${package}|${component}|${version}|${filename}|${description}|${date}" >> ${file_cache}
       fi
     else
       date=$(remote_file_get_last_modified ${server}/debian/${filename})
       echo "${package}|${component}|${version}|${filename}|${description}|${date}" >> ${file_cache}
     fi
   done
   ${compress} ${file_cache}
 done
done

store_current_timestamp "data-source-proposed-updates-update"
