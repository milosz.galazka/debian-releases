#!/bin/sh
# define distibutions
# check if ~oldoldstable~ distribution is removed from the Debian mirror network
# then specify all distributuions
if curl --output /dev/null --silent --head --fail  a${server}a/debian/dists/oldoldstable/Release; then
	# distributions to resolve
	distributions_all="unstable testing stable oldstable oldoldstable"

	# define distributions for security updates
	distributions_security="stable oldstable oldoldstable"

	# set global variable
	distributions_oldoldstable_is_removed=0
else
	# distributions to resolve
	distributions_all="unstable testing stable oldstable"

	# define distributions for security updates
	distributions_security="stable oldstable"

	# set global variable
	distribution_oldoldstable_is_removed=1
fi 

# define distributions to get updates
distributions_updates="stable oldstable"

# define distributions to get proposed updates
distributions_proposed_updates="stable oldstable"

# define distributions to get new packages
distributions_new="unstable testing"
