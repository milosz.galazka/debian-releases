#!/bin/sh
# configuration

# include server
. $path_settings/settings-server.sh

# include architecture
. $path_settings/settings-arch.sh

# include system components
. $path_settings/settings-components.sh

# include releases
. $path_settings/settings-releases.sh

# include packages
. $path_settings/settings-packages.sh

# include compression options
. $path_settings/settings-compression.sh
