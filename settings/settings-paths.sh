#!/bin/sh
# define paths
path_main="/home/milosz/debian-releases"
path_settings="${path_main}/settings"
path_cache="${path_main}/cache"
path_downloads="${path_main}/downloads"
path_timestamps="${path_main}/timestamps"
path_functions="${path_main}/functions"
path_output="${path_main}/output"
path_changelogs="${path_main}/changelogs"
