#!/bin/sh
# compression options
compress="xz --quiet --quiet --compress"
decompress_to_stdout="xz --quiet --quiet --decompress --stdout"
decompress_to_file="xz --quiet --quiet --decompress"
cext=".xz"
