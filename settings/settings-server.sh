#!/bin/sh
# define random Debian mirror to be used
case "$(shuf -i 1-4 -n 1)" in
    "1") server="http://ftp.pl.debian.org";;
    "2") server="http://ftp.de.debian.org";;
    "3") server="http://ftp.uk.debian.org";;
    "4") server="http://ftp.hu.debian.org";;
esac
