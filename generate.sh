#!/bin/sh
# generate web pages to populate "output" directory

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-output.sh

# generate html pages

# index
mkdir -p ${path_output}
(html_page_header "Home"
generate_section_brief_releases
generate_section_brief_security
generate_section_information
html_page_footer) > ${path_output}/index.html

# releases
directory="releases"
mkdir -p ${path_output}/${directory}
(html_page_header "Debian releases"
generate_section_releases
generate_section_information "data-source-releases-update"
html_page_footer) > ${path_output}/${directory}/index.html

# newly added
directory="newly-added"
mkdir -p ${path_output}/${directory}
(html_page_header "Packages added during the last 7 days"
generate_section_new_toc
generate_section_new_packages
generate_section_information "data-source-newly-added-update"
html_page_footer) > ${path_output}/${directory}/index.html

# package versions
directory="versions"
mkdir -p ${path_output}/${directory}
(html_page_header "Packages version comparison"
generate_section_package_versions_toc
generate_section_package_versions_table
generate_section_package_versions_descriptions
generate_section_firefox
generate_section_information "data-source-packages-update"
html_page_footer) > ${path_output}/${directory}/index.html

# updates
directory="updates"
mkdir -p ${path_output}/${directory}
(html_page_header "Updates"
generate_section_updates_toc
generate_section_security_updates
generate_section_updates
generate_section_proposed_updates
generate_section_information "data-source-updates-update"
html_page_footer) > ${path_output}/${directory}/index.html

# rss
directory="rss"
mkdir -p ${path_output}/${directory}
(html_page_header "RSS"
generate_section_rss
generate_section_information
html_page_footer) > ${path_output}/${directory}/index.html

