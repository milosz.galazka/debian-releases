#!/bin/sh
# update release files

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh

# local function
# get release file
get_release_file(){
  curl -o ${file_local} --silent ${file_remote}
  timestamp=$(local_file_get_last_modified ${file_local})
  ${compress} ${file_local}
  local_file_set_last_modified ${timestamp} ${file_local_compressed}
}

for distribution in $distributions_all; do
  file_remote="${server}/debian/dists/${distribution}/Release"
  file_local="${path_downloads}/release-${distribution}"
  file_local_compressed="${path_downloads}/release-${distribution}${cext}"

  if [ -f "${file_local_compressed}" -a "$(remote_file_get_last_modified ${file_remote})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
    # file exists and it is older then remote

    # get current release
    release_old=$(${decompress_to_stdout} ${file_local_compressed} | awk -v FS=": " '/Codename/ {print $2}')

    rm ${file_local_compressed} # delete old file
    get_release_file # download, compress, set "Last-Modified" time
    
    release_new=$(${decompress_to_stdout} ${file_local_compressed} | awk -v FS=": " '/Codename/ {print $2}')
    if [ "$release_new" != "$release_old" ]; then
      distibutions_changed=1
    fi
  elif [ ! -f "${file_local_compressed}" ]; then
    # file does not exists
   
    get_release_file # download, compress, set "Last-Modified" time
  fi
done

store_current_timestamp "data-source-releases-update"
