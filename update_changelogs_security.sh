#!/bin/sh
# update "security changelogs"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh
. $(dirname $0)/functions/functions-database.sh

# override server for changelogs
server="http://metadata.ftp-master.debian.org"

# local function
# get defined number of records
get_security_updates_top(){
  distribution=$1
  top_records=$2

  ${decompress_to_stdout} ${security_file_local_compressed} | sort -t "|" -k 6 -n -r | head -${top_records} | awk  -v FS="|" -v RS="\n" '{print $1}'
}

# get changelog
get_changelog(){
  if [ -f "${file_local}" ]; then
    unlink  ${file_local_compressed}
  else
    mkdir -p ${path_changelogs}/${directory}
  fi

  if curl --output /dev/null --silent --head --fail ${file_remote}; then
    curl -o ${file_local} --silent ${file_remote}
    timestamp=$(local_file_get_last_modified ${file_local})

    # store only first entry
    first_line=$(cat ${file_local} | head -1)
    real_package=$(echo ${first_line} | awk '{print $1}')
    sed -i -n "/^${first_line}/,/^${real_package}/ {/^${real_package}/!p}" ${file_local}

    ${compress} ${file_local}
    local_file_set_last_modified ${timestamp} ${file_local_compressed}
  fi
}

for distribution in $distributions_security; do
  for component in ${components}; do
    security_file_local_compressed="${path_cache}/security-updates-${distribution}-${component}-${arch}.xz"
    updated_packages=$(get_security_updates_top "$distribution" 100)

    if [ -n "${updated_packages}" ]; then
      for package in $updated_packages; do
        version=$(get_cached_field_for_package ${package} "version" ${security_file_local_compressed} | awk -F\: '{print $NF}')
        basefile=$(get_cached_field_for_package ${package} "file" ${security_file_local_compressed})
        directory=$(dirname ${basefile} | sed 's/pool\/updates//')

        package_file_local="${path_downloads}/packages-${distribution}-${arch}"
        package_file_local_compressed="${package_file_local}.xz"
        alt_package=$(extract_field_for_package ${package} "Source" ${package_file_local_compressed} | awk '{print $1}')
        alt_version=$(extract_field_for_package ${package} "Source" ${package_file_local_compressed} | sed  -n '/.*(.*)/ {s/.* (\(.*\))/\1/p}')

        if [ -n "${alt_package}" ]; then
          real_package=${alt_package}
        else
          real_package=${package}
        fi

        if [ -n "${alt_version}" ]; then
          real_version=${alt_version}
        else
          real_version=${version}
        fi

        pretty_version=$(echo ${real_version} | awk -F\: '{print $NF}')
        file_local="${path_changelogs}/${directory}/${real_package}_${pretty_version}_changelog"
        file_local_compressed="${file_local}.xz"
        file_remote="${server}/changelogs/${directory}/${real_package}_${pretty_version}_changelog"

        if [ -f ${file_local_compressed} ]; then
          if [ "$(remote_file_get_last_modified ${file_remote})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
            get_changelog
          fi
        elif [ ! -f ${file_local_compressed} ]; then
          get_changelog
        fi
      done
    fi
  done
done

store_current_timestamp "data-source-changelogs-security-update"
