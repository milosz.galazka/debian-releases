#!/bin/sh
# update "package versions"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh

# local function
# get alternate package file
get_alt_package_file(){
  curl -o ${file_local_compressed_alt} --silent ${file_remote_alt}
  timestamp=$(local_file_get_last_modified ${file_local_compressed_alt})
  gzip --force --decompress ${file_local_compressed_alt}
  if [ -f ${file_local_compressed} ]; then
    unlink  ${file_local_compressed}
  fi
  $compress ${file_local}
  local_file_set_last_modified ${timestamp} ${file_local_compressed}
}


for distribution in $distributions_all; do
  for component in $components_pkg; do
    file_remote="${server}/debian/dists/${distribution}/${component}/binary-${arch}/Packages.xz"
    file_remote_alt="${server}/debian/dists/${distribution}/${component}/binary-${arch}/Packages.gz"
    file_local="${path_downloads}/packages-${distribution}-${component}-${arch}"
    file_local_compressed="${file_local}.xz"
    file_local_compressed_alt="${file_local}.gz"

    if [ -f ${file_local_compressed} ]; then
      # file is already downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        # use .xz file
        if [ "$(remote_file_get_last_modified ${file_remote})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          unlink  ${file_local_compressed}
          curl -o ${file_local_compressed} --silent ${file_remote}
        fi
      else
        # use .gz file
        if [ "$(remote_file_get_last_modified ${file_remote_alt})" -ge "$(local_file_get_last_modified ${file_local_compressed})" ]; then
          get_alt_package_file
        fi
      fi
    elif [ ! -f ${file_local_compressed} ]; then
      # file is not downloaded
      if curl --output /dev/null --silent --head --fail ${file_remote}; then
        curl -o ${file_local_compressed} --silent ${file_remote}
      else
        get_alt_package_file
      fi
    fi
  done
done

store_current_timestamp "data-source-packages-update"
