#!/bin/sh
# update "newly-added packages"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-update-process.sh

for distribution in $distributions_new; do
  file_remote="https://packages.debian.org/${distribution}/main/newpkg?mode=byage&format=rss"
  file_local="${path_cache}/newly-added-${distribution}"
  file_local_compressed="${path_cache}/newly-added-${distribution}${cext}"

  # remove old file
  if [ -f "${file_local_compressed}" ]; then
    rm ${file_local_compressed}
  fi

  # get, parse and compress rss feed
  curl --silent ${file_remote} | grep -E '(title>|link>|description>)' | sed -e '1,3d' -e 'N;N;s/\n/ /g' -e 's/<.*>\(.*\)<.*> <.*>\(.*\)<.*> <.*>\(.*\)<.*>/\1|\2|\3/' > ${file_local}
  ${compress} ${file_local}

  # Cache format is: 
  #   package|url|description
  # Example:
  #   libtommath1|http://packages.debian.org/stretch/main/libtommath1|multiple-precision integer library [runtime]

done

store_current_timestamp "data-source-newly-added-update"
