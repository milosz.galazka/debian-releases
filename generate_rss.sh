#!/bin/sh
# update "package versions"

# import settings
. $(dirname $0)/settings/settings-paths.sh
. $(dirname $0)/settings/settings.sh

# import functions used during update process
. $(dirname $0)/functions/functions-output.sh


# http://www.xul.fr/en-xml-rss.html
# http://feedvalidator.org/check.cgi?url=https%3A%2F%2F213.167.242.184%2Frss%2Fstable-updates-main.rss

for distribution in ${distributions_updates}; do
  types="updates security-updates proposed-updates"
  for type in ${types}; do
    for component in $components; do
    (file_local_compressed="${path_cache}/${type}-${distribution}-${component}-${arch}.xz"

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    echo "<rss version=\"2.0\">"
    echo " <channel>"
    echo " <link>http://debian.sleeplessbeastie.eu/rss/${distribution}-${type}-${component}.rss</link>"
    #echo " <atom:link href=\"http://debian.sleeplessbeastie.eu//rss/${distribution}-${type}-${component}.rss\" rel=\"self\" type=\"application/rss+xml\" />"
    case ${type} in
      "updates")
        echo "  <title>Debian $(get_release_codename "$distribution" "pretty") ($distribution) updates</title>"
        ;;
      "proposed-updates")
        echo "  <title>Debian $(get_release_codename "$distribution" "pretty") ($distribution) proposed updates</title>"
        ;;
      "security-updates")
        echo "  <title>Debian $(get_release_codename "$distribution" "pretty") ($distribution) security updates</title>"
        ;;
    esac

    case ${type} in
      "updates")
        echo "  <description>Release updates for Debian $(get_release_codename "$distribution" "pretty") ($distribution)</description>"
        ;;
      "proposed-updates")
        echo "  <description>Proposed updates Debian $(get_release_codename "$distribution" "pretty") ($distribution)</description>"
        ;;
      "security-updates")
        echo "  <description>Security updates for Debian $(get_release_codename "$distribution" "pretty") ($distribution)</description>"
        ;;
    esac

    items=$(${decompress_to_stdout} ${file_local_compressed} | sort -t "|" -k 6 -n -r | head -30 | awk  -v FS="|" -v RS="\n" '{print $1}')
    for item in ${items}; do
      echo "   <item>"
      echo "    <guid isPermaLink=\"false\">${item}-$(get_cached_field_for_package ${item} "version" ${file_local_compressed})-$(get_cached_field_for_package ${item} "updated" ${file_local_compressed})</guid>"
      echo "    <title>${item} was updated on $(get_cached_field_for_package ${item} "updated_pretty" ${file_local_compressed})</title>"
      echo "    <pubDate>$(get_cached_field_for_package ${item} "updated_pretty" ${file_local_compressed})</pubDate>"
      echo "    <link>https://packages.debian.org/$(get_release_codename "$distribution")/${item}</link>"
      echo "    <description><![CDATA[<strong>${item}</strong> (<em>$(get_cached_field_for_package ${item} "description" ${file_local_compressed})</em>) was updated on <strong>$(get_cached_field_for_package ${item} "updated_pretty" ${file_local_compressed})</strong> to version <strong>$(get_cached_field_for_package ${item} "version" ${file_local_compressed})</strong>]]></description>"
      echo "   </item>"
    done
    echo " </channel>"
    echo "</rss>")>${path_output}/rss/${distribution}-${type}-${component}.rss
  done
  done
done
